let coursesData = [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Facilis exercitationem fugiat fuga architecto quod repellat officiis, impedit dolores asperiores voluptatem esse expedita, recusandae, necessitatibus magni maxime placeat sapiente quis cupiditate!",
        price: 25000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Phyton-Django",
        description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Facilis exercitationem fugiat fuga architecto quod repellat officiis, impedit dolores asperiores voluptatem esse expedita, recusandae, necessitatibus magni maxime placeat sapiente quis cupiditate!",
        price: 35000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java-Springboot",
        description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Facilis exercitationem fugiat fuga architecto quod repellat officiis, impedit dolores asperiores voluptatem esse expedita, recusandae, necessitatibus magni maxime placeat sapiente quis cupiditate!",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc004",
        name: "NodeJS-ExpressJS",
        description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Facilis exercitationem fugiat fuga architecto quod repellat officiis, impedit dolores asperiores voluptatem esse expedita, recusandae, necessitatibus magni maxime placeat sapiente quis cupiditate!",
        price: 55000,
        onOffer: false
    }
]

export default coursesData