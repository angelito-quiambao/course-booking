import {useState, useEffect} from 'react'
import {Card} from "react-bootstrap"

export default function CourseCard({courseProp}){
    // console.log(props)
    // console.log(courseProp)

    //first variable, second function
    let [count, setCount] = useState(0)
    let [seat, seatCount] = useState(30)

    const {name, description, price} = courseProp
    // console.log(name)
    // console.log(description)
    // console.log(price)

    useEffect(() => {
        console.log('render')
    }, [count])

    const handleClick = () =>{
        // console.log(`I'm clicked`, count + 1)
        
        // console.log(seat)
        if(seat > 0){
            setCount(count+1)
            seatCount(seat-1)
        }
        else{
            alert(`No more seats.`)
        }
    }
 
    return(
        <Card className="m-5">
        <Card.Body>
            <Card.Title className="font-weight-Bold">{name}</Card.Title>
            <Card.Subtitle className="font-weight-Bold">Description:</Card.Subtitle>
            <Card.Text>
                {description}
            </Card.Text>

            <Card.Subtitle className="font-weight-Bold mt-2">Price:</Card.Subtitle>
            <Card.Text className="font-weight-Bold mb-4">PhP {price}</Card.Text>

            <Card.Text>Count: {count}</Card.Text>

            <Card.Link className="btn btn-primary" onClick={handleClick}>Enroll</Card.Link>
        </Card.Body>
        </Card>
    )
}