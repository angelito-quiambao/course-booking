

export default function Banner({bannerProp}){

    const {Title, Paragraph, Button, Link} = bannerProp

    return(
        <div className="jumbotron jumbotron-fluid">
            <div className="container">
                <h1 className="display-4">{Title}</h1>
                <p className="lead">{Paragraph}</p>
                <a className="btn btn-info" href={Link}>{Button}</a>
            </div>
        </div>
    )
}