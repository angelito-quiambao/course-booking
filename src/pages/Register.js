import {useState, useEffect, useContext} from "react";
import {Form, Button, Row, Col, Container} from "react-bootstrap"
import {useNavigate} from 'react-router-dom'

import UserContext from './../UserContext'

export default function Register(){

    
    const navigate = useNavigate();

    const {user} = useContext(UserContext)
    const {id} = user

    const [fN, setFN] = useState("")
    const [lN, setLN] = useState("")
    const [email, setEmail] = useState("")
    const [pw, setPW] = useState("")
    const [vpw, setVPW] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    // useEffect(function, options)

    useEffect(() =>{
        id !== null
        ?
        navigate('/courses')
        :
        navigate('/register')

    }, [id])

    useEffect(() => {
        // console.log('render')
        if(fN === "" || lN === "" || email === "" || pw === "" || vpw === ""){
            setIsDisabled(true)
        }
        else{
            if(pw !== vpw){
                setIsDisabled(true)
            }
            else{
                setIsDisabled(false)
            }
        }

    }, [fN, lN, email, pw, vpw])

    const registerUser = (e) =>{
        e.preventDefault()
        // console.log(e)

        fetch('http://localhost:3007/api/users/email-exists', {
            method: "POST",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(response => response.json())
        .then(response => {
            // console.log(response)
            if(!response){
                fetch('http://localhost:3007/api/users/register', {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    firstName: fN,
                    lastName: lN,
                    email: email,
                    password: pw
                })
            })
            .then(response => response.json())
            .then(response=> {
                // console.log(response)

                if(response){
                    alert('Registration Successful')
                    navigate('/login')
                }
                else{
                    alert('Something went wrong. Please try again.')
                }
            })
            }
            else{
                alert(`User already exists`)
            }
        })
    }

    return(
        <Container className="m-5">
            <h3 className="text-center">Register</h3>
            <Row className="justify-content-center">
                <Col xs={12} md={6}>
                    <Form onSubmit={(e) => registerUser(e)}>
                        <Form.Group className="mb-3">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control type="text" value={fN} onChange={(e) => setFN(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control type="text" value={lN} onChange={(e) => setLN(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" value={pw} onChange={(e) => setPW(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Verify Password</Form.Label>
                            <Form.Control type="password" value={vpw} onChange={(e) => setVPW(e.target.value)}/>
                        </Form.Group>

                        <Button variant="info" type="submit" disabled={isDisabled}>Submit</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}