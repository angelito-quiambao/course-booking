import Banner from './../components/Banner'
import Highlights from './../components/Highlights';

import { Fragment } from "react";



export default function Home(){

    const bannerInfo ={
        Title: "Welcome to Booking App!",
        Paragraph: "Opportunities for everyone, everywhere.",
        Button: "Click Here",
        Link: "#"
    }

    return(
        <Fragment>
            <Banner bannerProp={bannerInfo}/>
            <Highlights />
        </Fragment>
    )
}