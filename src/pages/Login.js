import {useState, useEffect} from "react";
import {Form, Button, Row, Col, Container} from "react-bootstrap"
import {useNavigate} from 'react-router-dom'

export default function Login(){

    const [email, setEmail] = useState("")
    const [pw, setPW] = useState("")

    const [isDisabled, setIsDisabled] = useState(true)

    const navigate = useNavigate()

    useEffect(() => {
        console.log('render')

        if(email === "" || pw === ""){
            setIsDisabled(true)
        }
        else{
            setIsDisabled(false)
        }

    }, [email, pw])

    const loginUser = (e) =>{
        e.preventDefault()
        // console.log(e)

        fetch(`http://localhost:3007/api/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
						email: email,
						password: pw
					})
		})

		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				localStorage.setItem('token', response.token)
                localStorage.setItem('email', email)
                
				let token = localStorage.getItem('token')
				fetch(`http://localhost:3007/api/users/profile`, {
					method: "GET",
					headers:{
						"Authorization": `Bearer ${token}`
					}
				})

				.then(response => response.json())
				.then(response => {
					localStorage.setItem('id', response._id)
					localStorage.setItem('isAdmin', response.isAdmin)
					alert('Login successfully!')

                        setEmail("")
                        setPW("")

                        // console.log(email)
                        // console.log(pw)

                    navigate('/courses')
				})

			}
			else{
				alert('Something went wrong. Please check your email and password.')
			}
		})


    }

    return(
        <Container className="m-5 mx-auto">
            <h3 className="text-center mt-5">Login</h3>
            <Row className="justify-content-center my-5 mx-0">
                <Col xs={12} md={6}>
                    <Form onSubmit={(e) => loginUser(e)}>

                        <Form.Group className="mb-3">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" value={pw} onChange={(e) => setPW(e.target.value)}/>
                        </Form.Group>

                        <Button variant="info" type="submit" disabled={isDisabled}>Submit</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}