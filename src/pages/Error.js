import { Fragment } from "react"
import Banner from "../components/Banner"

export default function Error(){

    const bannerInfo ={
        Title: "Page Not Found",
        Paragraph: "Go back to the",
        Button: "Homepage",
        Link: "/"

    }

    return(
        <Fragment>
            <Banner bannerProp={bannerInfo}/>
        </Fragment>
    )
}